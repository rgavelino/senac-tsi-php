<?php

//CRIAR CLASSE
class Usuario {
    
    private $id;
    private $nome;
    private $email;
    private $senha;
    private $objDb;
    private $objDel;
    private $objListar;

    //CONTRUTOR DE CLASSE

    public function __construct(){

    //CONECTA NO BANCO DE DADOS    

    $this->objDb = new mysqli ('localhost','root','','aula_php',3307);

    echo "Deu Certo";    
    
    }

    //SET

    public function setId (int $id){
        $this -> id = $id;
    }
    
    public function setNome (string $nome){
        $this -> nome = $nome;
    }
    
    public function setEmail (string $email){
        $this -> email = $email;
    }
    
    public function setSenha (string $senha){
        $this -> senha = password_hash($senha, PASSWORD_DEFAULT);

    }

    //GET

    public function getId (int $id): int{
        return $this -> id;
    }
    
    public function getNome (string $nome): string{
       return $this -> nome;
    }
    
    public function getEmail (string $email): string{
        return $this -> email;
    }
    
    public function getSenha (string $senha): string{
        return $this -> senha;
    }

    public function getUser(int $id): int{
        return array($this -> id,
                     $this -> nome,
                     $this -> email);
    }

    //FUNÇÃO LISTAR

    public function listarUsuario(){

        $objStmt = $this->objDb->query("SELECT id, nome, email, senha FROM usuarios WHERE id = {$this->id}");

        return $objStmt;    

    }
    //FUNÇÃO DELETAR

    public function deletaUsuario(){

        $objStmt = $this->objDb->prepare('DELETE FROM usuarios WHERE id = ?');

        $objStmt->bind_param('i',
                             $this->id);

        if($objStmt->execute()){
            return true;
        }else{
            return false;
        }                    

    }

    //FUNÇÃO SALVR

    public function saveUsuario(){

        $objStmt = $this->objDb->prepare('REPLACE INTO usuarios(id, nome, email, senha) VALUES(?,?,?,?)');

        $objStmt->bind_param('isss', 
                            $this->id,
                            $this->nome,
                            $this->email,
                            $this->senha);

        if($objStmt->execute()){
            return true;
        }else{
            return false;
        }                    

    }



    public function __destruct(){
        
        unset ($this->objDb);


        echo "<br>Fechando a conexao com o banco";
    }    

}








