<?php


// //FUNÇÃO PARA GRAVAR LOGS NO SISTEMA
// function logger (string $str): bool{
//     $fp = fopen('log.txt', 'a');
//     if (fwrite($fp, $str)){
//         fclose($fp);
//         return true;
//     }else{
//         fclose($fp);
//         return false;
//     }
// }


//EXEMPLO COM MAIS DE UM PARAMETRO
function logger (string $str, int $nr_linha): bool{
    $fp = fopen('log.txt', 'a');
    if (fwrite($fp, $nr_linha . ':' .$str)){
        fclose($fp);
        return true;
    }else{
        fclose($fp);
        return false;
    }
}



// //EXEMPLO COM PARAMETRO NÃO OBRIGATÓRIO
// function logger (string $str, int $nr_linha = null): bool{
//     $fp = fopen('log.txt', 'a');
//     if (fwrite($fp, $nr_linha . ':' .$str)){
//         fclose($fp);
//         return true;
//     }else{
//         fclose($fp);
//         return false;
//     }
// }