<?php
session_start();
include_once("conexao.php");

$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
$result_usuario = "SELECT * FROM cliente WHERE id = '$id'";
$resultado_usuario = mysqli_query($conn, $result_usuario);
$row_usuario = mysqli_fetch_assoc($resultado_usuario);
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <title>CRUD - Editar Cliente</title>
    </head>
    <body>
        <a href="login.php">Login</a><br>
        <a href="cad_usuario.php">Cadastrar Cliente</a><br>
        <a href="cad_aeronave.php">Cadastrar Aeronave</a><br>
        <a href="index.php">Listar</a><br>
        <h1>Editar Cliente</h1>
        <?php
        if(isset($_SESSION['msg']))
            echo $_SESSION['msg'];
            unset ($_SESSION['msg']);
        ?>


            <form method="POST"action="proc_editar_usuario.php">

            <input type="hidden" name="id" id="" placeholder="Digite o nome" value="<?php echo $row_usuario['id'];?>">  <br><br>

            <label>Nome: </label>
            <input type="text" name="nome" id="" placeholder="Digite o nome" value="<?php echo $row_usuario['nome'];?>">  <br><br>
            
            <label>Email: </label>
            <input type="email" name="email" id="" placeholder="Digite o e-mail" value="<?php echo $row_usuario['email'];?>"><br><br>
            
            <label>C P F: </label>
            <input type="cpf" name="cpf" id="" placeholder="Digite o CPF" value="<?php echo $row_usuario['cpf'];?>"><br><br>
            
            <input type="submit" value="Editar">
    </form>
    </body>
    </html>