<?php

//CRIAR CLASSE
class Usuario {
    
    private $id;
    private $nome;
    private $email;
    private $senha;

    //CONTRUTOR DE CLASSE

    public function __construct(){
        
        echo "Aqui será feita a conexão com SGBD";
    }

    //SET

    public function setId (int $id){
        $this -> id = $id;
    }
    
    public function setNome (string $nome){
        $this -> nome = $nome;
    }
    
    public function setEmail (string $email){
        $this -> email = $email;
    }
    
    public function setSenha (string $senha){
        $this -> senha = $senha;
    }

    //GET

    public function getId (int $id): int{
        return $this -> id;
    }
    
    public function getNome (string $nome): string{
       return $this -> nome;
    }
    
    public function getEmail (string $email): string{
        return $this -> email;
    }
    
    public function getSenha (string $senha): string{
        return $this -> senha;
    }

    public function __destruct(){
        
        echo "<br>Fechando a conexao com o banco";
    }    

}








