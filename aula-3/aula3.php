<?php
echo "<pre>";
//Constante no PHP

define('QTDE_PAGINAS', 10);

//echo = escrever na tela

echo "Valor da minha constante é: " . QTDE_PAGINAS;

//Variavel para passar valor para uma constante

$ip_do_banco = '192.168.45.12';

define('ip_do_banco', $ip_do_banco);

echo "\nO IP SGBD é: " . ip_do_banco;

//Constantes mágicas

echo "\nEstou na linha: " . __LINE__;
echo "\nEstou na linha: " . __LINE__;
echo "\nEste é o arquivo: " . __FILE__;

//Muito bom para depurar o código

echo "\n\n";

var_dump($ip_do_banco); 

//VETORES

$dias_da_semana =['dom','seg','ter','qua','qui','sex','sab'];

$dias_da_semana =[1,2,3,4,5,6,7];

unset($dias_da_semana);//destroi a variavel

$dias_da_semana[0] ='dom';
$dias_da_semana[1] ='seg';
$dias_da_semana[2] ='ter';
$dias_da_semana[3] ='qua';
$dias_da_semana[4] ='qui';
$dias_da_semana[5] ='sex';
$dias_da_semana[6] ='sab';

$dias_da_semana = array(0 =>'dom',
                        1 =>'seg',
                        2 =>'ter',
                        3 =>'qua',
                        4 =>'qui',
                        5 =>'sex',
                        6 =>'sab');


echo "\n\n";
var_dump ($dias_da_semana);
