<?php
//echo("<pre>");

//Acesso remoto

//var_dump($_SERVER);
//echo "Seu IP é: " .$_SERVER["REMOTE_ADDR"];

//echo "\nSeu IP é: {$_SERVER['REMOTE_ADDR']}";

//Informações do PHP
//phpinfo();

//EXERCICIO 1

$alunos[0]['nome'] = 'Rodrigo Avelino';
$alunos[0]['bitbucket'] = 'https://bitbucket/rgavelino';
$alunos[1]['nome'] = 'Bruno Queiros';
$alunos[1]['bitbucket'] = 'https://bitbucket/bqdesigner';
$alunos[2]['nome'] = 'Felipe Lins';
$alunos[2]['bitbucket'] = 'https://bitbucket/felipelins';
$alunos[3]['nome'] = 'Carlos Almeida';
$alunos[3]['bitbucket'] = 'https://bitbucket/carlosalmeida';


for($i = 0; $i < count($alunos); $i++){
    echo $alunos[$i]['nome'] ."<br>"; 
    echo $alunos[$i]['bitbucket'] ."<br>"; ;
    echo('<br>');
}

echo "---------------------------------------------------------<br>";
//EXERCICIO 2   



$alunos2[0]['nome'] = 'Rodrigo Avelino';
$alunos2[0]['bitbucket'] = 'https://bitbucket/rgavelino';
$alunos2[1]['nome'] = 'Bruno Queiros';
$alunos2[1]['bitbucket'] = 'https://bitbucket/bqdesigner';
$alunos2[2]['nome'] = 'Felipe Lins';
$alunos2[2]['bitbucket'] = 'https://bitbucket/felipelins';
$alunos2[3]['nome'] = 'Carlos Almeida';
$alunos2[3]['bitbucket'] = 'https://bitbucket/carlosalmeida';

$i=0;
while($i < count($alunos2)){
    echo $alunos2[$i]['nome'] ."<br>"; 
    echo $alunos2[$i]['bitbucket'] ."<br>"; ;
    echo('<br>');
    $i++;
}

//--------------------------------------------------------------------------------------------------------------------------

//Outra forma

$aluno = array (0 => array ('nome' => 'Rodrigo Avelino',
                'bitbucket' => 'https://bitbucket/rgavelino'),
                 1 => array ('nome' => 'Bruno Queiros',
                'bitbucket' => 'https://bitbucket/bqdesigner'),      
                 2 => array ('nome' => 'Felipe Lins',
                'bitbucket' => 'https://bitbucket/felipelins'),  
                 3 => array ('nome' => 'Carlos Almeida',
                'bitbucket' => 'https://bitbucket/carlosalmeida'));  

echo '<table border=1>
        <thead>
            <th> Nome </th>
            <th> GitBucket </th>     
        </thead>';

for($i = 0; $i < count($aluno); $i++){
    echo '<tr>
            <td>{$aluno[$i]['nome']}</td>
            <td>{$aluno[$i]['bitbucket']}</td>
            </tr>';
}
echo'</table>';

echo "<br>";   

echo '<table border=1>
        <thead>
            <th> Nome </th>
            <th> GitBucket </th>     
        </thead>';

foreach($aluno as $id => $linha){
    echo "<tr>
            <td>{$linha['nome']}</td>
            <td>{$linha['bitbucket']}</td>
            </tr>";
}
echo'</table>';

